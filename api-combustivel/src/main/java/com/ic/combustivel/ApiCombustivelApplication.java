package com.ic.combustivel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiCombustivelApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiCombustivelApplication.class, args);
	}

}
