package com.ic.combustivel.VO;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MediaCombustivelVO {
	private String descricao;
	private BigDecimal mediaCompra;
	private BigDecimal mediaVenda;
}
