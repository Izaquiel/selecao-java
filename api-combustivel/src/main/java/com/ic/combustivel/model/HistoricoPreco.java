package com.ic.combustivel.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Digits;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@Getter
@Setter
public class HistoricoPreco {

	@Id
	@GeneratedValue(generator = "hist_preco_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "hist_preco_seq", sequenceName = "hist_preco_seq", initialValue = 1, allocationSize = 1)
	private Long id;

	@Column(name = "siglaRegiao", length = 2)
	private String siglaRegiao;

	@Column(name = "estadoSigla", length = 2)
	private String siglaEstado;

	@Column(name = "municipio", length = 100)
	private String municipio;

	@Column(name = "revenda", length = 150)
	private String revenda;

	@Column(name = "codInstalacao", length = 20)
	private String codInstalacao;

	@Column(name = "produto", length = 50)
	private String produto;

	@Column(name = "dataColeta")
	private LocalDate dataColeta;

	@Digits(integer = 4, fraction = 4)
	@Column(name = "valorCompra")
	private BigDecimal valorCompra;

	@Digits(integer = 4, fraction = 4)
	@Column(name = "valorVenda")
	private BigDecimal valorVenda;

	@Column(name = "unidadeMedida", length = 20)
	private String unidadeMedida;

	@Column(name = "bandeira", length = 100)
	private String bandeira;

}
