package com.ic.combustivel.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.ic.combustivel.model.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long>{

	@Query("SELECT u FROM Usuario u WHERE LOWER(u.nome) = LOWER(:nome)")
	public Usuario buscarPorNome(@Param("nome") String nome);
}
