package com.ic.combustivel.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.ic.combustivel.model.HistoricoPreco;

public interface HistoricoPrecoRepository extends CrudRepository<HistoricoPreco, Long> {

	@Query("SELECT hp.produto, AVG(hp.valorVenda), AVG(hp.valorCompra) FROM HistoricoPreco hp "
			+ "WHERE LOWER(hp.municipio) LIKE LOWER(:municipio) GROUP BY hp.produto")
	public List<Object[]> getMediaPrecoVendaECompraCombustivel(@Param("municipio") String municipio);

	@Query("SELECT hp.municipio, AVG(hp.valorVenda), AVG(hp.valorCompra) FROM HistoricoPreco hp "
			+ " GROUP BY hp.municipio")
	public List<Object[]> getMediaPrecoVendaECompra();

	@Query("SELECT hp.bandeira, AVG(hp.valorVenda), AVG(hp.valorCompra) FROM HistoricoPreco hp "
			+ " GROUP BY hp.bandeira")
	public List<Object[]> getMediaPrecoVendaECompraBandeira();

	@Query("SELECT hp FROM HistoricoPreco hp ORDER BY hp.bandeira")
	public List<HistoricoPreco> getDadosAgrupadosPorDistribuidora();

	@Query("SELECT hp FROM HistoricoPreco hp ORDER BY hp.dataColeta")
	public List<HistoricoPreco> getDadosAgrupadosDataColeta();
	
	@Query("SELECT hp FROM HistoricoPreco hp ORDER BY hp.siglaRegiao")
	public List<HistoricoPreco> getDadosImportadosPorRegiao();
}
