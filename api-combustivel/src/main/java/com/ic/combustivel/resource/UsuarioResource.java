package com.ic.combustivel.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ic.combustivel.model.Usuario;
import com.ic.combustivel.repositories.UsuarioRepository;

@RestController
public class UsuarioResource {

	@Autowired
	private UsuarioRepository repository;

	public UsuarioResource() {
	}

	@RequestMapping(value = "/usuario", method = RequestMethod.GET)
	public ResponseEntity<List<Usuario>> listarTodos() {
		List<Usuario> usuarios = (List<Usuario>) repository.findAll();
		return new ResponseEntity<List<Usuario>>(usuarios, HttpStatus.OK);
	}

	@RequestMapping(value = "/usuario", method = RequestMethod.POST)
	public ResponseEntity<Usuario> salvar(@RequestBody Usuario usuario) {
		Usuario u = repository.save(usuario);
		return new ResponseEntity<Usuario>(u, HttpStatus.OK);
	}

	@RequestMapping(value = "/usuario", method = RequestMethod.PUT)
	public ResponseEntity<Usuario> atualizar(@RequestBody Usuario usuario) {
		Usuario retorno = repository.save(usuario);
		return new ResponseEntity<Usuario>(retorno, HttpStatus.OK);
	}

	@RequestMapping(value = "/usuario/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Usuario> deletarPorId(@PathVariable("id") Long id) {
		Optional<Usuario> temp = repository.findById(id);
		if (temp.isPresent()) {
			repository.deleteById(id);
			return new ResponseEntity<Usuario>(HttpStatus.NO_CONTENT);
		}

		return new ResponseEntity<Usuario>(HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(value = "/usuario/{id}", method = RequestMethod.GET)
	public ResponseEntity<Usuario> buscarPorId(@PathVariable("id") Long id) {
		Optional<Usuario> temp = repository.findById(id);
		if (temp.isPresent()) {
			return new ResponseEntity<Usuario>(temp.get(), HttpStatus.NO_CONTENT);
		}

		return new ResponseEntity<Usuario>(HttpStatus.NOT_FOUND);
	}
}
