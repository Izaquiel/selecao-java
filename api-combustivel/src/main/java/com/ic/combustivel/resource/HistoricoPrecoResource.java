package com.ic.combustivel.resource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ic.combustivel.VO.MediaCombustivelVO;
import com.ic.combustivel.model.HistoricoPreco;
import com.ic.combustivel.repositories.HistoricoPrecoRepository;

@RestController
public class HistoricoPrecoResource {

	@Autowired
	private HistoricoPrecoRepository repository;

	public HistoricoPrecoResource() {
	}

	@RequestMapping(value = "/historicoPreco", method = RequestMethod.GET)
	public ResponseEntity<List<HistoricoPreco>> listarTodos() {
		List<HistoricoPreco> historicos = (List<HistoricoPreco>) repository.findAll();
		return new ResponseEntity<List<HistoricoPreco>>(historicos, HttpStatus.OK);
	}

	@RequestMapping(value = "/historicoPreco", method = RequestMethod.POST)
	public ResponseEntity<HistoricoPreco> salvar(@RequestBody HistoricoPreco historico) {
		HistoricoPreco hp = repository.save(historico);
		return new ResponseEntity<HistoricoPreco>(hp, HttpStatus.OK);
	}

	@RequestMapping(value = "/historicoPreco", method = RequestMethod.PUT)
	public ResponseEntity<HistoricoPreco> atualizar(@RequestBody HistoricoPreco historico) {
		HistoricoPreco hp = repository.save(historico);
		return new ResponseEntity<HistoricoPreco>(hp, HttpStatus.OK);
	}

	@RequestMapping(value = "/historicoPreco/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<HistoricoPreco> deletarPorId(@PathVariable("id") Long id) {
		Optional<HistoricoPreco> temp = repository.findById(id);
		if (temp.isPresent()) {
			repository.deleteById(id);
			return new ResponseEntity<HistoricoPreco>(HttpStatus.NO_CONTENT);
		}

		return new ResponseEntity<HistoricoPreco>(HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(value = "/historicoPreco/{id}", method = RequestMethod.GET)
	public ResponseEntity<HistoricoPreco> buscarPorId(@PathVariable("id") Long id) {
		Optional<HistoricoPreco> temp = repository.findById(id);
		if (temp.isPresent()) {
			return new ResponseEntity<HistoricoPreco>(temp.get(), HttpStatus.OK);
		}

		return new ResponseEntity<HistoricoPreco>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(value = "/historicoPreco/mediaCombustivel/{municipio}", method = RequestMethod.GET)
	public ResponseEntity<List<MediaCombustivelVO>> buscarMediaCombustivelPorMunicipio(
			@PathVariable("municipio") String municipio) {
		List<Object[]> objetos = (List<Object[]>) repository.getMediaPrecoVendaECompraCombustivel(municipio);
		List<MediaCombustivelVO> medias = new ArrayList<>();
		for (Object[] obj : objetos) {
			MediaCombustivelVO aux = new MediaCombustivelVO();
			aux.setDescricao(obj[0] == null ? null : obj[0].toString());
			aux.setMediaVenda(obj[1] == null ? null : new BigDecimal(obj[1].toString()));
			aux.setMediaCompra(obj[2] == null ? null : new BigDecimal(obj[2].toString()));
			medias.add(aux);
		}
		return new ResponseEntity<List<MediaCombustivelVO>>(medias, HttpStatus.OK);
	}

	@RequestMapping(value = "/historicoPreco/mediaCombustivelMunicipio", method = RequestMethod.GET)
	public ResponseEntity<List<MediaCombustivelVO>> buscarMediaCombustivelPorMunicipio() {
		List<Object[]> objetos = (List<Object[]>) repository.getMediaPrecoVendaECompra();
		List<MediaCombustivelVO> medias = new ArrayList<>();
		for (Object[] obj : objetos) {
			MediaCombustivelVO aux = new MediaCombustivelVO();
			aux.setDescricao(obj[0] == null ? null : obj[0].toString());
			aux.setMediaVenda(obj[1] == null ? null : new BigDecimal(obj[1].toString()));
			aux.setMediaCompra(obj[2] == null ? null : new BigDecimal(obj[2].toString()));
			medias.add(aux);
		}
		return new ResponseEntity<List<MediaCombustivelVO>>(medias, HttpStatus.OK);
	}

	@RequestMapping(value = "/historicoPreco/mediaCombustivelBandeira", method = RequestMethod.GET)
	public ResponseEntity<List<MediaCombustivelVO>> buscarMediaCombustivelPorBandeira() {
		List<Object[]> objetos = (List<Object[]>) repository.getMediaPrecoVendaECompraBandeira();
		List<MediaCombustivelVO> medias = new ArrayList<>();
		for (Object[] obj : objetos) {
			MediaCombustivelVO aux = new MediaCombustivelVO();
			aux.setDescricao(obj[0] == null ? null : obj[0].toString());
			aux.setMediaVenda(obj[1] == null ? null : new BigDecimal(obj[1].toString()));
			aux.setMediaCompra(obj[2] == null ? null : new BigDecimal(obj[2].toString()));
			medias.add(aux);
		}
		return new ResponseEntity<List<MediaCombustivelVO>>(medias, HttpStatus.OK);
	}

	@RequestMapping(value = "/historicoPreco/regiao", method = RequestMethod.GET)
	public ResponseEntity<List<HistoricoPreco>> buscarAgrupadoRegiao() {
		List<HistoricoPreco> retorno = (List<HistoricoPreco>) repository.getDadosImportadosPorRegiao();
		return new ResponseEntity<List<HistoricoPreco>>(retorno, HttpStatus.OK);
	}

	@RequestMapping(value = "/historicoPreco/distribuidora", method = RequestMethod.GET)
	public ResponseEntity<List<HistoricoPreco>> buscarAgrupadoDistribuidora() {
		List<HistoricoPreco> retorno = (List<HistoricoPreco>) repository.getDadosAgrupadosPorDistribuidora();
		return new ResponseEntity<List<HistoricoPreco>>(retorno, HttpStatus.OK);
	}

	@RequestMapping(value = "/historicoPreco/dataColeta", method = RequestMethod.GET)
	public ResponseEntity<List<HistoricoPreco>> buscarAgrupadoDataColeta() {
		List<HistoricoPreco> retorno = (List<HistoricoPreco>) repository.getDadosAgrupadosDataColeta();
		return new ResponseEntity<List<HistoricoPreco>>(retorno, HttpStatus.OK);
	}

	@RequestMapping(value = "/upload", consumes = "multipart/form-data", method = RequestMethod.POST)
	public ResponseEntity<String> uploadMultipart(@RequestParam("file") MultipartFile file) throws IOException {
		InputStream in = file.getInputStream();
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String line = null;
		br.readLine();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		List<HistoricoPreco> historicos = new ArrayList<>();
		while ((line = br.readLine()) != null) {
			String[] split = line.split("  ");

			HistoricoPreco hp = new HistoricoPreco();
			hp.setSiglaRegiao(split[0].trim());
			hp.setSiglaEstado(split[1].trim());
			hp.setMunicipio(split[2]);

			String revenda = "";
			int i;

			for (i = 3; i < split.length; i++) {
				try {
					Integer.parseInt(split[i]);
					break;
				} catch (NumberFormatException e) {
					revenda += split[i] + " ";
				}
			}

			hp.setRevenda(revenda.replaceAll("  ", " "));
			hp.setCodInstalacao(split[i]);
			hp.setProduto(split[i + 1]);

			String dataAux = split[i + 2];
			LocalDate date = LocalDate.parse(dataAux, dtf);
			hp.setDataColeta(date);

			String compra = split[i + 3];
			if (StringUtils.isNotBlank(compra)) {
				compra = compra.replace(",", ".");
				hp.setValorCompra(new BigDecimal(compra));
			}

			String venda = split[i + 4];
			if (StringUtils.isNotBlank(venda)) {
				venda = venda.replace(",", ".");
				hp.setValorVenda(new BigDecimal(venda));
			}

			hp.setUnidadeMedida(split[i + 5]);
			hp.setBandeira(split[i + 6]);

			System.out.println(hp.toString());
			historicos.add(hp);
		}

		repository.saveAll(historicos);
		br.close();
		return new ResponseEntity<String>("Arquivo lido com sucesso", HttpStatus.OK);
	}

}
