
POST http://localhost:8080/historicoPreco  HTTP/1.1
content-type: application/json

{
    "siglaRegiao": "NE",
    "siglaEstado": "CE",
    "municipio": "Brejo Santo",
    "revenda": "Posto 3",
    "codInstalacao": "1234",
    "produto": "Gasolina",
    "dataColeta": "2018-04-20",
    "valorCompra": 4.711,
    "valorVenda": 4.835,
    "unidadeMedida": "REAIS_POR_LITRO",
    "bandeira": "Petrobras SA"
}

###

GET http://localhost:8080/historicoPreco HTTP/1.1

###

GET http://localhost:8080/historicoPreco/10 HTTP/1.1

###

PUT http://localhost:8080/historicoPreco HTTP/1.1
content-type: application/json

{
    "id": 10,
    "siglaRegiao": "NE",
    "siglaEstado": "CE",
    "municipio": "Brejo Santo",
    "revenda": "Posto Independete",
    "codInstalacao": "1234",
    "produto": "Gasolina",
    "dataColeta": "2018-04-20",
    "valorCompra": 4.411,
    "valorVenda": 4.635,
    "unidadeMedida": "REAIS_POR_LITRO",
    "bandeira": "Petrobras SA"
}

###

DELETE http://localhost:8080/historicoPreco/10 HTTP/1.1

###

GET http://localhost:8080/historicoPreco/mediaCombustivel/brejo santo HTTP/1.1

###

GET http://localhost:8080/historicoPreco/mediaCombustivelMunicipio HTTP/1.1

###

GET http://localhost:8080/historicoPreco/mediaCombustivelBandeira HTTP/1.1

###

GET http://localhost:8080/historicoPreco/regiao HTTP/1.1

###

GET http://localhost:8080/historicoPreco/distribuidora HTTP/1.1

###

GET http://localhost:8080/historicoPreco/dataColeta HTTP/1.1


